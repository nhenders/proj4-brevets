"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
from datetime import datetime

SPEEDS = {200: {"close": 15, "open": 34}, 
        400: {"close": 15, "open": 32}, 
        600: {"close": 15, "open": 30}, 
        1000: {"close": 11.428, "open": 28}, 
        1300: {"close": 13.333, "open": 26}}
DISTANCES = list(SPEEDS)


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    
    # for checkpoint no. 1
    if control_dist_km == 0:
        return brevet_start_time 

    return calc_time(control_dist_km, brevet_dist_km, brevet_start_time, "open")


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    # "oddities", paragraph 1: rusa.org/pages/acp-brevet-control-times-calculator
    if control_dist_km == 0:
        return arrow.get(brevet_start_time).shift(hours=+1).isoformat()

    return calc_time(control_dist_km, brevet_dist_km, brevet_start_time, "close")



def calc_time(control_dist_km, brevet_dist_km, brevet_start_time, key):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
       key:  string, "open" or "close" for whichever time is desired
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    # if the control distance is greater than the total brevet, use the control
    # rule from rusa.org
    if brevet_dist_km <= control_dist_km:
        control_dist_km = brevet_dist_km

    shift, curr_dist, i = 0,0,0

    while(True):
        if control_dist_km < DISTANCES[i]:
            control_dist_km -= curr_dist
            shift += control_dist_km / SPEEDS[DISTANCES[i]][key]
            break
        else:
            shift += (DISTANCES[i]-curr_dist) / SPEEDS[DISTANCES[i]][key]
            curr_dist = DISTANCES[i]
            if control_dist_km == curr_dist:
                break
            i += 1

    s_hours = int(shift)
    s_minutes = round(60*(shift-s_hours))
    
    return arrow.get(brevet_start_time).shift(hours=+s_hours,minutes=+s_minutes).isoformat()

