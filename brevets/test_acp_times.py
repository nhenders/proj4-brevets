'''
test_acp_times.py
Nick Henderson

Test functions for acp_times.py. Intended to be automated
with nose.

'''

import datetime
import acp_times
from datetime import datetime
import arrow

START = datetime(2019, 1, 1, 0, 0).isoformat()

def test_open_norm():
    ''' Normal use case for open '''
    assert acp_times.open_time(100.0, 200, START) == arrow.get(2019, 1, 1, 2, 56).isoformat()

def test_close_norm():
    ''' Normal use case for close '''
    assert acp_times.close_time(100.0, 200, START) == arrow.get(2019, 1, 1, 6, 40).isoformat()    

def test_small():
    ''' Control immediately after start'''
    assert acp_times.open_time(1.0, 200, START) == arrow.get(2019, 1, 1, 0, 2).isoformat()
    assert acp_times.close_time(1.0, 200, START) == arrow.get(2019, 1, 1, 0, 4).isoformat()

def test_open_start():
    ''' 1000km brevet, test based on rusa.org first checkpoint open'''
    assert acp_times.open_time(0.0, 1000, START) == START
    
def test_close_start():
    ''' 1000km brevet, test based on rusa.org first checkpoint close '''
    assert acp_times.close_time(0.0, 1000, START) == arrow.get(2019, 1, 1, 1, 0).isoformat()

