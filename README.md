# Project 4: Brevet Time Calculator

Reimplemented RUSA ACP controle time calculator using flask and ajax.

Control times are calculated using the logic described here (https://rusa.org/pages/acp-brevet-control-times-calculator). It is assumed that all distances entered are valid control points, so the last one is not >+/-20% from the brevet distance. Per rusa.org, small discrepancies between the final control point and the brevet distance do not affect the open/close times. 

The close time for the starting control (0km) is 1 hour after the start time. See paragraph 1 of "oddities" in the link above.

This calculator takes input using jQuery, then processes the information using flask and ajax. Output is reflected immediately.

# Contact

Nick Henderson

nhenders@uoregon.edu / nick@nihenderson.com
